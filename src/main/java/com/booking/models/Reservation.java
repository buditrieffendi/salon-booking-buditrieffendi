package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
    private static int number = 1;
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = generateIdReservation();
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    }

    private double calculateReservationPrice(){
        double totalPrice = 0;
        double price = 0;
        double diskon = 0;

        switch (customer.getMember().getMembershipName()){
            case "Silver":
                diskon = (double) 5 /100;
                break;
            case "Gold":
                diskon = (double) 10 /100;
                break;
        }

        for (Service data: services){
            price = price + data.getPrice();
        }

        totalPrice = price - (price * diskon);

        return totalPrice;
    }

    private String generateIdReservation(){
        String format = "Res-%03d";
        String result = String.format(format, number);
        number++;
        return result;
    }
}
