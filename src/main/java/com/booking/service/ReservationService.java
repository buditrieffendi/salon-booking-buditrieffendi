package com.booking.service;


import com.booking.models.*;

import java.util.List;
import java.util.Scanner;

public class ReservationService {
    public static void createReservation(List<Reservation> reservationList, List<Person> personList, List<Service> serviceList, Scanner input){
        Customer customer = ValidationService.validateCustomerId(personList, input);
        Employee employee = ValidationService.validateEmployeeId(personList, input);
        List<Service> listService = ValidationService.validateServiceId(serviceList, input);

        Reservation reservation = new Reservation(customer, employee, listService, "In Process");

        reservationList.add(reservation);
        System.out.println("Booking Berhasil");
        System.out.println("Total Biaya Booking : " + reservation.getReservationPrice());
    }

    public static void getCustomerByCustomerId(){
        
    }

    public static void editReservationWorkstage(List<Reservation> reservationList, Scanner input){
        Reservation reservation = ValidationService.validateReservationId(reservationList, input);

        String finishOrCancel = ValidationService.finisOrCancel(input);

        reservation.setWorkstage(finishOrCancel);
        System.out.println();

        System.out.println("Reservasi dengan id "+ reservation.getReservationId() +" sudah "+ finishOrCancel);
    }

    public static double getKeuntungan(List<Reservation> reservationList){
        double totalKeuntungan = 0;
        for (Reservation data: reservationList){
            if (data.getWorkstage().equalsIgnoreCase("Finish")){
                totalKeuntungan = totalKeuntungan + data.getReservationPrice();
            }
        }
        return totalKeuntungan;
    }

}
