package com.booking.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.booking.models.*;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Person> listAllPerson){
        System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("+========================================================================================+");

        List<Customer> listAllCustomer = listAllPerson.stream()
                .filter(data-> data instanceof Customer)
                .map(data-> (Customer)data)
                .collect(Collectors.toList());

        IntStream.range(0, listAllCustomer.size())
                .forEach(index-> {
                    Customer customer = listAllCustomer.get(index);
                    int num = index + 1;
                    System.out.printf("| %-4s | %-10s | %-15s | %-15s | %-15s | %-15s |\n",
                            num, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), customer.getWallet());
                });

    }

    public static void showAllEmployee(List<Person> listAllPerson){
        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+========================================================================================+");

        List<Employee> listAllEmployee = listAllPerson.stream()
                .filter(data-> data instanceof Employee)
                .map(data-> (Employee)data)
                .collect(Collectors.toList());

        IntStream.range(0, listAllEmployee.size())
                .forEach(index-> {
                    Employee employee = listAllEmployee.get(index);
                    int num = index + 1;
                    System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |\n",
                            num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                });
        
    }

    public void showHistoryReservation(List<Reservation> reservationList){
        System.out.printf("| %-4s | %-4s | %-15s | %-25s | %-20s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+===========================================================================================================================+");

        List<Reservation> historyReservationList = reservationList.stream()
                .filter(data-> data.getWorkstage().equalsIgnoreCase("Finish") || data.getWorkstage().equalsIgnoreCase("Canceled" ))
                .collect(Collectors.toList());

        IntStream.range(0, historyReservationList.size())
                .forEach(index-> {
                    Reservation reservation = historyReservationList.get(index);
                    int num = index + 1;
                    System.out.printf("| %-4s | %-4s | %-15s | %-25s | %-20s | %-15s | %-15s |\n",
                            num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                });

        System.out.printf("| %-57s | %-56s |\n",
                "Total Keuntungan", "Rp." + ReservationService.getKeuntungan(reservationList));
        System.out.println("+===========================================================================================================================+");

    }

    public static void showAllService(List<Service> serviceList){
        System.out.printf("| %-4s | %-4s | %-20s | %-15s |\n",
                "No.", "ID", "Nama", "Harga");
        System.out.println("+========================================================================================+");

        IntStream.range(0, serviceList.size())
                .forEach(index-> {
                    Service service = serviceList.get(index);
                    int num = index + 1;
                    System.out.printf("| %-4s | %-4s | %-20s | %-15s |\n",
                            num, service.getServiceId(), service.getServiceName(), service.getPrice());
                });

    }
}
