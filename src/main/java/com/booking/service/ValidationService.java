package com.booking.service;

import com.booking.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static void validateInput(){

    }

    public static Customer validateCustomerId(List<Person> personList, Scanner input){
        PrintService.showAllCustomer(personList);
        Optional<Customer> hasilCariCustomer;
        boolean isLoop = false;
        do{
            System.out.print("Masukkan ID Customer: ");
            String idCustomer = input.nextLine();

            hasilCariCustomer = personList.stream()
                    .filter(person -> person instanceof Customer && person.getId().equals(idCustomer))
                    .map(data-> (Customer) data)
                    .findFirst();

            Customer customer = hasilCariCustomer.orElse(null);

            if(hasilCariCustomer.isPresent()){
                isLoop = true;
            }else if (customer == null){
                System.out.println("ID Customer tidak valid. Coba lagi.");
            }
        }while (!isLoop);
        return hasilCariCustomer.get();
    }

    public static Employee validateEmployeeId(List<Person> personList, Scanner input){
        PrintService.showAllEmployee(personList);
        Optional<Employee> hasilCariEmployee;
        boolean isLoop = false;
        do{
            System.out.print("Masukkan ID Employee: ");
            String idEmployee = input.nextLine();

            hasilCariEmployee = personList.stream()
                    .filter(person -> person instanceof Employee && person.getId().equals(idEmployee))
                    .map(data-> (Employee) data)
                    .findFirst();

            Employee employee = hasilCariEmployee.orElse(null);

            if(hasilCariEmployee.isPresent()){
                isLoop = true;
            }else if (employee == null){
                System.out.println("ID Employee tidak valid. Coba lagi.");
            }
        }while (!isLoop);
        return hasilCariEmployee.get();
    }

    public static List<Service> validateServiceId(List<Service> serviceList, Scanner input){
        PrintService.showAllService(serviceList);
        Optional<Service> hasilCariService;
        List<Service> pilihService = new ArrayList<>();;
        boolean isLoop = false;

        do{
            System.out.print("Masukkan ID Service: ");
            String idService = input.nextLine();

            hasilCariService = serviceList.stream()
                    .filter(service -> service.getServiceId().equals(idService))
                    .findFirst();

            Service service = hasilCariService.orElse(null);

            if(hasilCariService.isPresent()){
                pilihService.add(hasilCariService.get());
                isLoop = validasiTambahService(input);
            }else if (service == null){
                System.out.println("ID Service tidak valid. Coba lagi.");
            }
        }while (!isLoop);

        return pilihService;
    }
    public static Reservation validateReservationId(List<Reservation> reservationList, Scanner input){
        PrintService.showRecentReservation(reservationList);
        Optional<Reservation> hasilCariReservation;
        boolean isLoop = false;
        do{
            System.out.print("Masukkan ID Reservation: ");
            String idReservation = input.nextLine();

            hasilCariReservation = reservationList.stream()
                    .filter(reservation -> reservation.getReservationId().equals(idReservation))
                    .findFirst();

            Reservation reservation = hasilCariReservation.orElse(null);

            if(hasilCariReservation.isPresent()){
                isLoop = true;
                System.out.println(hasilCariReservation.get());
            }else if (reservation == null){
                System.out.println("ID Reservation tidak valid. Coba lagi.");
            }
        }while (!isLoop);
        return hasilCariReservation.get();
    }

    public static boolean validasiTambahService(Scanner input){
        System.out.println("Ingin pilih service yang lain (Y/T)?");
        String isLanjut = input.nextLine();
        boolean result = false;
        switch (isLanjut){
            case "Y":
            case "y":
                result = false;
                break;
            case "T":
            case "t":
                result = true;
                break;
            default:
                System.out.println("Input tidak valid");
        }
        return result;
    }

    public static String finisOrCancel(Scanner input){
        String result = null;
        do {
            System.out.println("Selesaikan Reservasi: (Finish/Cancel)");
            String finishOrCancel = input.nextLine();
            if (finishOrCancel.equalsIgnoreCase("finish")){
                result = "Finish";
            }else if (finishOrCancel.equalsIgnoreCase("cancel")){
                result = "Cancel";
            }
        }while (result == null);
        return result;
    }
}
